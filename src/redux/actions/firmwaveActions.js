import { getFirmwaveFilespi } from '../../api/firmwaveApi';

export const GET_FIRMWAVE_FILES = 'GET_FIRMWAVE_FILES';

export const getFirmwaveFiles = () => {
  return (dispatch) => {
    getFirmwaveFilespi().then((res) => {
      dispatch({
        type: GET_FIRMWAVE_FILES,
        payload: res.data.data,
      });
    });
  };
};
