import themeReducer from './themeReducer';
import sidebarReducer from './sidebarReducer';
import firmwaveReducer from './firmwaveReducer';

export {
  themeReducer, sidebarReducer, firmwaveReducer,
};
