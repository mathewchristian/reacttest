import { GET_FIRMWAVE_FILES } from '../actions/firmwaveActions';

const initialState = { data: '' };

export default function (state = initialState, action) {
  switch (action.type) {
    case GET_FIRMWAVE_FILES:
      return { ...state, data: action.payload };
    default:
      return state;
  }
}
