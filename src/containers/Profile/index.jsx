import React from 'react';
import { Col, Container, Row } from 'reactstrap';
import UserDetailForm from './components/UserDetailForm';
const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;

const ProfileDetails = () => (
  <Container className="dashboard">
    <Row>
      <Col md={12}>
        <h3 className="page-title">User Details</h3>
      </Col>
    </Row>
    <Row>
      <img className="topbar__avatar-img profile" src={Ava} alt="avatar" />
    </Row>
    <Row>
      <Col>
        <UserDetailForm onSubmit/>
      </Col>
    </Row>
  </Container>
);

export default ProfileDetails;
