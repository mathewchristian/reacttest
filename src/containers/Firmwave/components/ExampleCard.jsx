import React from 'react';
import { Card, CardBody, Col, Row } from 'reactstrap';
import { withRouter } from 'react-router-dom';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { getFirmwaveFiles } from '../../../redux/actions/firmwaveActions';
const Ava = `${process.env.PUBLIC_URL}/img/ava.png`;

class ExampleCard extends React.Component {
  static propTypes = {
    dispatch: PropTypes.func.isRequired
  };

  componentDidMount(){
    const { dispatch } = this.props;
    dispatch(getFirmwaveFiles());
  }

  render (){
    const { firmwave } = this.props;
    console.log(firmwave.data.sections)
    return(
      <Col md={12}>
        <Card>
          <CardBody>
            <div className="card__title">
              <img className="topbar__avatar-img" src={Ava} alt="avatar" />
              <h5 className="bold-text">{firmwave && firmwave.data.sections ?  firmwave.data.sections[0].title : 'File Title'}</h5>
            </div>
            <Row >
              <Col >
                <h6>File</h6>
                  <p>{firmwave && firmwave.data.sections &&  firmwave.data.sections[0].firmwareFiles.length ?  firmwave.data.sections[0].firmwareFiles[0] : 'firmwaveFile.tar.ga'}</p>
              </Col>
              <Col >
                <h6>Date</h6>
                  <p>{'2/7/2019'}</p>
              </Col>
            </Row>
          </CardBody>
        </Card>
      </Col>
    )
  }
}

export default withRouter(connect(state => ({
  firmwave: state.firmwaves
}))(ExampleCard));


