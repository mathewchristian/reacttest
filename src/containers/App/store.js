import { combineReducers, createStore, applyMiddleware } from 'redux';
import { reducer as reduxFormReducer } from 'redux-form';
import thunk from 'redux-thunk';
import {
  sidebarReducer, themeReducer, firmwaveReducer,
} from '../../redux/reducers/index';

const reducer = combineReducers({
  form: reduxFormReducer, // mounted under "form",
  theme: themeReducer,
  sidebar: sidebarReducer,
  firmwaves: firmwaveReducer,
});

const store = createStore(
  reducer,
  applyMiddleware(thunk),
);

export default store;
