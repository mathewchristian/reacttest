import React, { Component } from 'react';
import PropTypes from 'prop-types';
import SidebarLink from './SidebarLink';

class SidebarContent extends Component {
  static propTypes = {
    onClick: PropTypes.func.isRequired,
  };

  hideSidebar = () => {
    const { onClick } = this.props;
    onClick();
  };

  render() {
    return (
      <div className="sidebar__content">
        <ul className="sidebar__block">
            <SidebarLink title="Welcome" route="/log_in" onClick={this.hideSidebar} />
            <SidebarLink title="Documentation" route="/pages/Doumentation" onClick={this.hideSidebar} />
            <SidebarLink title="Frimwave" route="/pages/Firmwave" onClick={this.hideSidebar} />
        </ul>
      </div>
    );
  }
}

export default SidebarContent;
