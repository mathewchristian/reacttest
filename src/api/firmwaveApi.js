import axios from 'axios';

export function getFirmwaveFilespi() {
  return new Promise((resolve, reject) => {
    setTimeout(() => {
      const URL = 'http://localhost:9000/api/firmwave/getall';
      try {
        const res = axios.get(`${URL}`);
        resolve(res);
      } catch (e) {
        reject(new Error('Something went wrong'));
      }
    }, 500);
  });
}
